package com.example.freemarker.dto;

import com.example.freemarker.controller.UserController;
import com.fasterxml.jackson.annotation.JsonFormat;
import org.springframework.format.annotation.DateTimeFormat;

import java.time.LocalDate;

public class UserDto {

    private int numberRegister;
    private String name;

    @DateTimeFormat(pattern = "yyyy-MM-dd")
    private LocalDate activeDate;

    private boolean admin;

    public UserDto() {
    }

    public int getNumberRegister() {
        return numberRegister;
    }

    public void setNumberRegister(int numberRegister) {
        this.numberRegister = numberRegister;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public LocalDate getActiveDate() {
        return activeDate;
    }

    public void setActiveDate(LocalDate activeDate) {
        this.activeDate = activeDate;
    }

    public boolean isAdmin() {
        return admin;
    }

    public void setAdmin(boolean admin) {
        this.admin = admin;
    }
}
