package com.example.freemarker.dto;

public class ToogleDto {

    private boolean account;
    private boolean shopping;
    private boolean favorite;
    private boolean torpedoes;

    public boolean isAccount() {
        return account;
    }

    public void setAccount(boolean account) {
        this.account = account;
    }

    public boolean isShopping() {
        return shopping;
    }

    public void setShopping(boolean shopping) {
        this.shopping = shopping;
    }

    public boolean isFavorite() {
        return favorite;
    }

    public void setFavorite(boolean favorite) {
        this.favorite = favorite;
    }

    public boolean isTorpedoes() {
        return torpedoes;
    }

    public void setTorpedoes(boolean torpedoes) {
        this.torpedoes = torpedoes;
    }
}
