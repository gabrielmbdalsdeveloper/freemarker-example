package com.example.freemarker.repository;

import com.example.freemarker.dto.AccountDto;
import com.example.freemarker.dto.FavoriteDto;
import com.example.freemarker.dto.ShoppingDto;
import com.example.freemarker.dto.TorpedoesDto;
import com.example.freemarker.util.TemplateUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.util.ResourceUtils;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.util.HashMap;
import java.util.Map;

@Component
public class TemplateRepository {

    @Autowired
    private TemplateUtil templateUtil;

    public String loadTemplate(){
        try {
            File file = ResourceUtils.getFile("classpath:templates/json/porteira.txt");
            return new String(Files.readAllBytes(file.toPath()));
        } catch (IOException e) {
            e.printStackTrace();
        }
        return "";
    }

    public String find() {
        String template = loadTemplate();
        Map<String, Object> input = fillObjects();
        return templateUtil.fillTemplateResponse(input,template);
    }

    private Map<String, Object> fillObjects() {
        Map<String, Object> input = new HashMap<>();
        input.put("account",verifyAccount());
        input.put("shopping",verifyShopping());
        input.put("favorite",verifyFavorite());
        input.put("torpedoes",verifyTorpedoes());
        return input;
    }

    private TorpedoesDto verifyTorpedoes() {
        TorpedoesDto torpedoesDto = new TorpedoesDto();
        torpedoesDto.setActive(true);
        torpedoesDto.setDescription("Envie torpedos e ganhe <bold>10%</bold> de descontos em jatos.");
        torpedoesDto.setIcon("http://host/folder/torpedoes.png");
        torpedoesDto.setTitle("Torpedos");
        return torpedoesDto;
    }

    private FavoriteDto verifyFavorite() {
        FavoriteDto favoriteDto = new FavoriteDto();
        favoriteDto.setActive(true);
        favoriteDto.setDescription("Seus favoritos <bold>valem mais<bold> acesse para concorrer a prêmios.");
        favoriteDto.setIcon("http://host/folder/favorite.png");
        favoriteDto.setTitle("Favoritos");
        return favoriteDto;
    }

    private ShoppingDto verifyShopping() {
        ShoppingDto shoppingDto = new ShoppingDto();
        shoppingDto.setActive(true);
        shoppingDto.setDescription("Veja todos os itens da sua lista de compras");
        shoppingDto.setIcon("http://host/folder/shopping-list.png");
        shoppingDto.setTitle("Lista de compras");
        return shoppingDto;
    }

    private AccountDto verifyAccount() {
        AccountDto accountDto = new AccountDto();
        accountDto.setActive(true);
        accountDto.setDescription("Acesse os dados da sua conta");
        accountDto.setIcon("http://host/folder/account.png");
        accountDto.setTitle("Minha Conta");
        return accountDto;
    }

}
