package com.example.freemarker.controller;

import com.example.freemarker.dto.UserDto;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;

import java.util.ArrayList;
import java.util.List;

@Controller
@RequestMapping("user")
public class UserController {

    public static List<UserDto> userList = new ArrayList<>();

    @GetMapping
    public ModelAndView userPage(ModelAndView model){
        UserDto userDto = new UserDto();
        userDto.setNumberRegister(UserController.userList.size()+1);
        model.addObject("user",userDto);
        model.addObject("userList", userList);
        model.setViewName("userTemplate");
        return model;
    }

    @PostMapping("add")
    public ModelAndView addUser(@ModelAttribute UserDto user){
        userList.add(user);
        return new ModelAndView("redirect:/user");
    }
}
