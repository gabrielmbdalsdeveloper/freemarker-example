package com.example.freemarker.controller;

import com.example.freemarker.repository.TemplateRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/template")
public class GateController {

    @Autowired
    private TemplateRepository templateRepository;

    @GetMapping
    public String findTemplateByScreenId(){
        return templateRepository.find();
    }
}
