package com.example.freemarker.util;

import freemarker.cache.StringTemplateLoader;
import freemarker.template.Configuration;
import freemarker.template.DefaultObjectWrapperBuilder;
import freemarker.template.Template;
import org.springframework.stereotype.Component;
import org.springframework.ui.freemarker.FreeMarkerTemplateUtils;

import java.io.IOException;
import java.util.Map;

@Component
public class TemplateUtil {

    private static final String DEFAULT_TEMPLATE_NAME = "TEMPLATEBFF";

    public String fillTemplateResponse(Map<String, Object> input, String contentTemplate) {
        try{
            final Template template = createTemplate(contentTemplate);
            return FreeMarkerTemplateUtils.processTemplateIntoString(template, input);
        } catch (Exception e){
            e.printStackTrace();
        }
        return null;
    }

    private Template createTemplate(String contentTemplate) throws IOException {
        Configuration configuration = createTemplateConfigurate();
        configuration.setTemplateLoader(prepareTemplate(contentTemplate));
        return configuration.getTemplate(DEFAULT_TEMPLATE_NAME);
    }

    private Configuration createTemplateConfigurate() {
        Configuration configuration = new Configuration(Configuration.VERSION_2_3_30);
        configuration.setDefaultEncoding("UTF-8");
        configuration.setObjectWrapper(new DefaultObjectWrapperBuilder(Configuration.VERSION_2_3_30).build());
        return configuration;
    }

    private StringTemplateLoader prepareTemplate(String contentTemplate){
        StringTemplateLoader loader = new StringTemplateLoader();
        loader.putTemplate(DEFAULT_TEMPLATE_NAME, contentTemplate);
        return loader;
    }

}
